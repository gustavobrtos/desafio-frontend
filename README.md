O projeto foi construido em cima da biblioteca  [React](https://pt-br.reactjs.org)

O projeto hospedado funciona como uma plataforma de e-commerce 

Ele acessa a api pública do github para encontrar os membros de uma organização e listá-los como "produtos", 
sendo assim você pode adicionar a carrinho uma quantidade de qualquer produto assim como remover uma unidade por 
vez ou todos do mesmo produto.

Para ver os itens que foram escolhidos incluidos ao carrinho, no menu superior existe uma aba chamada "carrinho de compras" qua lista tudo 
o que foi inserido dentro dele, e disponibiliza opções de remove-los, adicionar mais uma unidade e tirar uma unidade.

Na página de carrinho é possivel finalizar a compra com todos os produtos selecionados escolhendo uma "opção de pagamento"

## Comandos necessários

Dentro do diretório do projeto você pode rodá-lo

### `npm start`

Rode o projeto no modo de desenvolvimento 
Abra [http://localhost:3000](http://localhost:3000) para visualizar no seu navegador.

Caso algum problema aconteça com depedências será necessário para o projeto e rodar o seguinte comando: 

### `npm install`

e logo em seguida apenas execute o comando para rodar em modo de desenvolvimento


import React, { Component } from 'react'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"

import { Input } from "antd"

import * as searchActions from "../actions/searchActions"

const { Search } = Input

class SearchRegisters extends Component {
  
  handleSearchRegisters(value){
    
    this.props.fetchRegisters(value)

  }
  
  render() {

    return (
      <React.Fragment>
        <h1>Digite uma empresa para buscar os membros</h1>
        <Search
          // style={{maxWidth: "430px"}}
          placeholder={"Digite uma empresa para buscar os membros"}
          enterButton={"Buscar"}
          size={"large"}
          onSearch={this.handleSearchRegisters.bind(this)}
        />
      </React.Fragment>

    )
  }
}


const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    fetchRegisters: searchActions.fetchRegisters
  }, dispatch)

} 



export default connect(null, mapDispatchToProps)(SearchRegisters)  

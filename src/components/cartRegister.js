import React, { Component } from "react"
import {connect} from "react-redux"
import { bindActionCreators } from "redux"

import { List, Layout, Avatar, Button, Modal, Alert, Row, Col, Typography, Radio } from "antd"
import { PlusOutlined, MinusOutlined, DeleteOutlined } from '@ant-design/icons'
import * as cartActions from "../actions/cartActions"

const {Title} = Typography

const { Content} = Layout


class CartRegister extends Component {

  constructor(props){
    super(props)

    this.state = { 
      modalState: false,
      checkboxState: 1 
    }
  }

  checkout(){
    this.props.clearCart()
    this.setState({modalState: false})

  }

  openModal(){
    this.setState({modalState: true})
  }

  closeModal(){
    this.setState({modalState: false})
  }

  onChangeCheckBox(e){
    this.setState({checkboxState: e.target.value})
  }

  render() {

    let finalValue = this.props.registerListData.registers.reduce((acc, curre) => {
      return (curre.id * curre.quantity) + acc
    }, 0)


    return (

        <Content style={{ padding: '0 50px' }}> 

          <Modal
            visible={this.state.modalState}
            title={"Deseja confirmar a sua compra? "}
            onCancel={this.closeModal.bind(this)}
            onOk={this.checkout.bind(this)}
            okText={"Finalizar compra"}
            cancelText={"Cancelar"}
          > 

             <List
              header={<div><Title level={5}> Itens adicionados: </Title>  </div>}
              footer={ <div> <Title level={5}> Valor final: {finalValue} </Title> </div> }
              itemLayout="horizontal"
              dataSource={this.props.registerListData.registers}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar src={item.avatar} />}
                    title={item.login}
                    description={`Preço total ${item.quantity * item.id}`}
                  />
                </List.Item>
              )}
            />

            <div> Selecione o método de pagamento: </div>

            <Radio.Group onChange={this.onChangeCheckBox.bind(this)} value={this.state.checkboxState}>
              <Radio value={1}>Cartão de crédito</Radio>
              <Radio value={2}>Boleto</Radio>
            </Radio.Group>

          </Modal>

          <Row justify={"center"}>
          {this.props.registerListData.registers.length === 0 ? 
            <Col span={12}>
              <Alert
                style={{marginTop: "25px"}}
                description="Carrinho de compras ainda vazio, busque alguns produtos e adicione-os para realizar a compra"
                type="info"
                showIcon
              />
            </Col>

             :

            <React.Fragment>
              <Col span={12}>
                <h1>Carrinho de compras</h1>
                <List
                  footer={ <div> <Title level={5}> Valor final: {finalValue} </Title> </div> }
                  itemLayout="horizontal"
                  dataSource={this.props.registerListData.registers}
                  renderItem={item => (
                    <List.Item
                    actions={[
                      <Button type={"primary"} 
                        key={"deleteToCart"} 
                        icon={<PlusOutlined />}
                        onClick={() => this.props.addToCart(item)}
                      />, 
                      <Button type={"primary"} 
                        key={"deleteToCart"} 
                        icon={<MinusOutlined />}
                        onClick={() => this.props.removeToCart(item)}
                      />,
                      <Button danger type={"primary"} 
                        key={"deleteToCart"} 
                        icon={<DeleteOutlined />}
                        onClick={() => this.props.deleteToCart(item)}
                      />
                    ]}
                  >

                    <List.Item.Meta
                      avatar={
                        <Avatar src={item.avatar} />
                      }
                      title={item.login}
                      description={` 
                        Preço unitário: ${item.id} 
                        Quantidade ${item.quantity}
                      `}
                    />
                    <div>Preço total {item.quantity * item.id}</div>

                    </List.Item>
                    
                    )
                  }
                />
                <Button 
                onClick={this.openModal.bind(this)} 
                type={"primary"}
                >
                Finalizar compra
              </Button>
            </Col>

            </React.Fragment>
            
          }

          </Row>

        </Content>

    )
  }
}

const mapStateToProps = (state) => ({
  registerListData:  state.registersCart
})

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({

    addToCart: cartActions.addToCart,
    removeToCart: cartActions.removeToCart,
    deleteToCart: cartActions.deleteToCart,
    clearCart: cartActions.clearCart

  }, dispatch)

} 


export default  connect(mapStateToProps, mapDispatchToProps)(CartRegister)  

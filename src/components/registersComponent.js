import React, { Component } from "react"
import {connect} from "react-redux"
import { bindActionCreators } from "redux"

import { Card, Avatar, Alert, Row, Col, Layout} from "antd"
import { PlusOutlined, MinusOutlined, DeleteOutlined } from '@ant-design/icons'

import SearchRegisters from "./searchRegisters"

import * as cartActions from "../actions/cartActions"

const { Meta } = Card
const { Content} = Layout

class RegistersComponent extends Component {


  render() {

    const membersItem = this.props.registers.map((item) => {

      return (
          
          <Col className="gutter-row" span={4}>
            
            <Card

              style={{marginBottom: "15px"}}
              actions={[
                <PlusOutlined onClick={() => this.props.addToCart(item) } key="addToCart" />,
                <MinusOutlined onClick={() => this.props.removeToCart(item) } key="removeToCart" />,
                <DeleteOutlined onClick={() => this.props.deleteToCart(item) } key="deleteToCart" />,
              ]}
            >
              <Meta
                avatar={<Avatar src={item.avatar_url} />}
                title={item.login}
                description={` Preço: R$ ${item.id} `}
              />
            </Card>

          </Col>   
        

      )
    })

    return (

          <Content style={{ padding: '0 50px' }}>
            
            <Row justify={"center"}>
              <Col span={12}>
                <SearchRegisters/>
              </Col>
            </Row>
             
            
              {
                this.props.error ? <Alert
                  description="Erro ao buscar membros da organização, digite uma que seja válida "
                  type="error"
                  showIcon
                /> :  
                <Row gutter={16}>
                  {membersItem}
                </Row>
                
              }
          </Content>


    )
  }
}

const mapStateToProps = (state) => ({
  registers: state.registersList.registers,
  error: state.registersList.error
})

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    addToCart: cartActions.addToCart,
    removeToCart: cartActions.removeToCart,
    deleteToCart: cartActions.deleteToCart
  }, dispatch)

} 


export default  connect(mapStateToProps, mapDispatchToProps)(RegistersComponent)  

import React, {Component} from 'react';

// import './App.css';

import {Provider} from "react-redux"

import { Layout, Menu, Badge } from 'antd'

import RegistersComponent from "./components/registersComponent"
import CartRegister from "./components/cartRegister"

import { ShoppingCartOutlined, HomeOutlined } from '@ant-design/icons'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

import store from "./store"

import * as cartActions from "./actions/cartActions"

const { Header } = Layout;

class App extends Component{


  render(){

    store.subscribe(store.getState)

    let activeTab = window.location.pathname === "/" ?  "1" : "2" 

    return (

      <Provider store={store}>

        <Layout className="layout">

          <Header>
        
            <Router>

              <Menu defaultSelectedKeys={[activeTab]} theme="dark" mode="horizontal" >
                <Menu.Item key="1">  <Link to={"/"}/> <HomeOutlined /> Página inicial </Menu.Item>
                <Menu.Item key="2"> <Link to={"/cart"}/>  <ShoppingCartOutlined />  Carrinho de compras </Menu.Item>
              </Menu>

              <Switch>
                <Route exact path={"/"} component={RegistersComponent}/>
                <Route path={"/cart"} component={CartRegister}/>
              </Switch>
              
            </Router>

          </Header>
          
        </Layout>
       
      </Provider>

    )
  }
}


export default App

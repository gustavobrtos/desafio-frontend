import * as actions from "../actions/actionTypes"


const initialState = {
  registers: [],
  error: false
}


function registersListReducer(state = initialState, action) {

  switch (action.type) {
    case actions.FETCH_REGISTERS: {

      let currentRegisters = action.payload.registers
      let apiError = action.payload.error
      
      return {
        ...state,
        registers : [...currentRegisters],
        error: apiError
        
      }

    }

    case actions.ERROR_REGISTERS: {

      let apiError = action.payload.error
      
      return {
        ...state,
        error: apiError,

        
      }


    }
  
    default: {
      return {
        ...state
      }
    }
  }


}


export default registersListReducer



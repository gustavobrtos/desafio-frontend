import * as actions from "../actions/actionTypes"


const initialState = {
  registers: []
}


function registersCartReducer(state = initialState, action) {

  switch (action.type) {
    case actions.ADD_TO_CART: {


      let registerPayload = action.payload.productData
      let currentRegisters = state.registers
      let newRegistersData = []

      let indexOfItem = currentRegisters.findIndex(element =>  element.login === registerPayload.login) 

      if(indexOfItem === -1){

        newRegistersData = [
          ...currentRegisters,
          {
            login: registerPayload.login,
            quantity: 1,
            id: registerPayload.id,
            avatar: registerPayload.avatar_url,
            url: registerPayload.html_url
          }
        ]

      }else{

        currentRegisters[indexOfItem].quantity += 1

        newRegistersData = [
          ...currentRegisters
        ]

      }

      return {
        ...state,
        registers: [...newRegistersData]
        
      }

    }

    case actions.REMOVE_TO_CART: {
      
      let registerPayload = action.payload.productData
      let currentRegisters = state.registers
      let newRegistersData = []

      let indexOfItem = currentRegisters.findIndex(element =>  element.login === registerPayload.login) 

      if(indexOfItem > -1){


        if(currentRegisters[indexOfItem].quantity === 1){

          currentRegisters.splice(indexOfItem, 1)

          newRegistersData = [
            ...currentRegisters
          ]


        }else{
          currentRegisters[indexOfItem].quantity -= 1

          newRegistersData = [
            ...currentRegisters
          ]
        }

      }else{

        newRegistersData = [
          ...currentRegisters,

        ]
      }


      return {
        ...state,
        registers: [...newRegistersData]
        
      }


    }

    case actions.DELETE_TO_CART: {


      let registerPayload = action.payload.productData
      let currentRegisters = state.registers
      let newRegistersData = []

      let indexOfItem = currentRegisters.findIndex(element =>  element.login === registerPayload.login) 

      currentRegisters.splice(indexOfItem, 1)

        
      newRegistersData = [
        ...currentRegisters
      ]

      
      return {
        ...state,
        registers: [...newRegistersData]
      }


    }

    case actions.CLEAR_CART: {

      return {
        ...state,
        registers: []
      }


    }
  
    default: {
      return {
        ...state
      }
    }
  }


}


export default registersCartReducer



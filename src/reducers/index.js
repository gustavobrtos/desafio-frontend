import  {combineReducers} from "redux"
import registersListReducer from "./registersListReducer"
import registersCartReducer  from  "./cartRegistersReducer"

export default combineReducers({
  registersList: registersListReducer,
  registersCart: registersCartReducer
})
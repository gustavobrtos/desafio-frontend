import * as actions from "./actionTypes"


export function fetchRegisters(company) {

  return (dispatch) => {
    
    fetch(`https://api.github.com/orgs/${company}/public_members`)
    .then((res) => res.json())
    .then((registers) => dispatch({
      type: actions.FETCH_REGISTERS,
      payload: {registers, error: false} 
    }))
    .catch((error) => dispatch ({
      type: actions.ERROR_REGISTERS,
      payload: { error: true} 
    }))


  }

}
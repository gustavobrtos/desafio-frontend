import * as actions from "./actionTypes"
import { message} from 'antd';

export function addToCart(productData) {
  
  message.success({ content: 'Produto adicionado ao carrinho', 
    style: {
        marginTop: '10vh',
      },
    }, 5
  )

  return (dispatch) => {

    dispatch({
      type: actions.ADD_TO_CART,
      payload: {
        productData
      }
    })
    
  }

}

export function removeToCart(productData) {

  message.success({ content: `Retirado 1 unidade de ${productData.login} do carrinho`, 
    style: {
        marginTop: '10vh',
      },
    }, 5
  )

  return (dispatch) => {

    dispatch({
      type: actions.REMOVE_TO_CART,
      payload: {
        productData
      }
    })
    
  }

}

export function deleteToCart(productData) {

  message.success({ content: `${productData.login} removido do carrinho`, 
    style: {
        marginTop: '10vh',
      },
    }, 5
  )

  return (dispatch) => {

    dispatch({
      type: actions.DELETE_TO_CART,
      payload: {
        productData
      }
    })
    
  }

}


export function clearCart() {
  message.success({ content: "Compras realizadas com succeso", 
    style: {
        marginTop: '10vh',
      },
    }, 5
  )

  return (dispatch) => {

    dispatch({
      type: actions.CLEAR_CART
    })
    
  }
}